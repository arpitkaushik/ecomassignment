import Order from '../models/order';
const ObjectId = require('mongodb').ObjectID;
async function orderById(req,res)
{
  try{
    const returnObj = {
      success: true,
      message: 'success',
      data: {},
    };

    const orderbyId = await Order.find({_id:ObjectId(req.params.orderId)}).populate({model : 'product',path: 'productIds'});
    if(orderbyId){
        returnObj.data = orderbyId;
        return res.send(returnObj);
    }else{
        returnObj.success = false;
        returnObj.message = 'No order Found';
        return res.send(returnObj);
    }
    
    }catch (error){
        console.log(error);
    }
}

async function addOrder(req,res)
{
  try{
    const returnObj = {
      success: true,
      message: 'success',
      data: {},
    };

    const add = new Order({
        name: req.body.name,
        address: req.body.address,
        productIds:req.body.productIds,
        totalPrice : req.body.totalPrice
      });
    const inserted = await add.saveAsync();
    if(inserted){
        returnObj.data = inserted._id;
        return res.send(returnObj);
    }else{
        returnObj.success = false;
        returnObj.message = 'Something went wrong';
        return res.send(returnObj);
    }
    
    }catch (error){
        console.log(error);
    }
}


async function orderAction(req,res)
{
  try{
    const returnObj = {
      success: true,
      message: 'success',
      data: {},
    };

    
    const action = await Order.updateOne({_id:req.body.orderId},{$set:{ orderStatus: req.body.action}}).execAsync();
    console.log(action);
    if(action.nModified == 1){
        return res.send(returnObj);
    }else{
        returnObj.success = false;
        returnObj.message = 'Something went wrong';
        return res.send(returnObj);
    }
    
    }catch (error){
        console.log(error);
    }
}

export default {
    orderById,
    addOrder,
    orderAction
  }