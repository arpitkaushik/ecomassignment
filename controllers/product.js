import Product from '../models/product';

async function allproduct(req,res)
{
  try{
    const returnObj = {
      success: true,
      message: 'success',
      data: {},
    };

    const allProduct = await Product.find({});
    if(allProduct.length > 0){
        returnObj.data = allProduct;
        return res.send(returnObj);
    }else{
        returnObj.success = false;
        returnObj.message = 'No product Found';
        return res.send(returnObj);
    }
    
    }catch (error){
        console.log(error);
    }
}

export default {
    allproduct,
  }