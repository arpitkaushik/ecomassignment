import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const orderSchema = new Schema ({
	name : { 
    	type : String,
        required:true
    },
    address : {
        type : String,
        default : ''
    },
    totalPrice : {
        type : Number,
        required:true
    },
    productIds : [],
    orderStatus : {
        type : String,
        enum: ['A', 'P', 'C','D'],//accept,pending,complete,declined
        default : 'P'
    }

},{timestamps: true});


 const orderModel = mongoose.model('order',orderSchema);
 export default orderModel;