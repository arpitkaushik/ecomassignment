import express from 'express';
import bodyParser from 'body-parser';
import config from './config/env';
import path from 'path';
import routes from './routes';
import HTTP from 'http';
import Promise, { getNewLibraryCopy } from 'bluebird';
import mongoose from 'mongoose';
import Product from './models/product';
const app = express();
const PORT = config.port;


const server = HTTP.createServer(app);

// promisify mongoose
Promise.promisifyAll(mongoose);

// connect to mongo db
mongoose.connect(config.db, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true
  }, () => {
    console.log(`connected to database`)
    var arr = [
      {
        name : "iphone X",
        description : "Apple iphone X",
        price : 61000 
      },
      {
        name : "Samsung note 10",
        description : "samsung note 10",
        price : 71000 
      }
    ];
    Product.insertMany(arr,function(err,docs){
      console.log(err,docs);
    })
  });
// If the connection throws an error
mongoose.connection.on('error',function (err) {  
  console.log('Mongoose default connection error: ' + err);
}); 

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {  
  console.log('Mongoose default connection disconnected'); 
});

// If the Node process ends, close the Mongoose connection 
process.on('SIGINT', function() {  
  mongoose.connection.close(function () { 
    //client.quit();
    console.log('Mongoose default connection disconnected through app termination'); 
    process.exit(0); 
  }); 
}); 

mongoose.set('debug', true);


app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());

app.get('/',(req,res) => {
  res.json({success:true});

});

app.use('/api/v1',routes);

server.listen(PORT,() => {
    console.log(`App is running on ${PORT}`);
});