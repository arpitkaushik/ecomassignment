import express from 'express';
import productRoutes from './product';
import orderRoutes from './order';
const router = express.Router();



router.use('/product',productRoutes);
router.use('/order',orderRoutes);



export default router;