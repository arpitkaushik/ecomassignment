import express from 'express';
import productCtrl from '../controllers/product';
const router = express.Router();


router.get('/all',productCtrl.allproduct);

export default router;
