import express from 'express';
import orderCtrl from '../controllers/order';
const router = express.Router();


router.get('/order/:orderId',orderCtrl.orderById);
router.post('/addorder',orderCtrl.addOrder);
router.put('/orderaction',orderCtrl.orderAction);

export default router;
